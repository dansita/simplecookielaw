/*
    +----------------------------------------------------------+
    |               Simple Cookie Law - v. 1.0.0               |
    |     Developed by Daniel Sedmak (contact@dansedmak.com)   |
    +----------------------------------------------------------+

Copyright (c) 2016 Daniel Sedmak

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
       
*/
console.log("Simple Cookie Law - v. 1.0.0\nCreated by Daniel Sedmak (contact@dansedmak.com)");
// Function to get the Cookie Name
function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin !== 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }
    return unescape(dc.substring(begin + prefix.length));
}
// Function to control if cookies exist
function openCookie(){
  var myCookie = getCookie("sessCook");

    if (myCookie == null) {
loadCookieCSS();      
createCookieDiv();
document.getElementById("simpleCookieBanner").style.display = "block";
    }
}
// Function to set the cookie
function closeCookie(){
document.getElementById("simpleCookieBanner").style.display = "none";
  document.cookie="sessCook=true; expires=Fri, 20 Jan 2017 12:00:00 UTC; path=/";
}
// Function to load the CSS for the cookie DIV
function loadCookieCSS(){
var cssId = 'simpleCookieLawCSSID';
if (!document.getElementById(cssId))
{
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.id   = cssId;
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = 'http://imprendo.pro/js/simpleCookieLaw/simpleCookieLaw.css';
    link.media = 'all';
    head.appendChild(link);
}
}
// Function to create the cookie div
function createCookieDiv(){
    // Create the new div
    var cookieDiv = document.createElement('div');
    cookieDiv.style.fontFamily = 'sans-serif';
    // Assigning it an ID
    cookieDiv.id = 'simpleCookieBanner';
    // Append the new div to the page
    document.body.appendChild(cookieDiv);
    document.getElementById("simpleCookieBanner").innerHTML = "<p>Questo sito utilizza dei cookies per offrirti una migliore esperienza di navigazione nel sito. Proseguendo la navigazione, accetti il loro utilizzo. <a href=\"#\" id=\"cookieMore\" onclick=\"cookieMore()\">Scopri di pi&ugrave;</a></p><a href\"javascript:void();\" id=\"simpleCookieOkButton\" onclick=\"closeCookie()\">D'accordo</a>";
}  

var GoogleAnalytics = false;
var HotJar = false;
var Magento = false;

function loadAllCookies(){
    if(checkCookie("_ga") != null){
        GoogleAnalytics = true;
    }
    if(checkCookie("_hjUserId") != null){
        HotJar = true;
    }
    /*
    if(checkCookie("_hjUserId") != null){
        HotJar = true;
    }
    */
}

function checkCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin !== 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }
    return unescape(dc.substring(begin + prefix.length));
}

function cookieMore(){
    loadAllCookies();
    var current_url = "<b>"+window.location.hostname+"</b>";
    var cookies = "";
    if (GoogleAnalytics == true){cookies = cookies + "<li>Google Analytics (Statistiche in forma anonima)</li>"}
    if (HotJar == true){cookies = cookies + "<li>HotJar (studio anonimo su come viene usato il sito)</li>"} 
    var cookuse = "";
    if(cookies != ""){
        cookuse = "<b>Alcuni cookie che stiamo usando in questo sito sono:</b><ul>"+cookies+"</ul>";
    }
    var x = document.getElementById("simpleCookieBanner");
    var txt = "<div style=\"width: 70%; max-width: 800px; margin: 0 auto; border-bottom: solid 1px #fff; margin-bottom: 10px; background: rgba(0, 0, 0, 0.20) none repeat scroll 0% 0%; text-align: left !important; padding: 0px 20px 0px 20px; max-height: 70%; overflow: auto; margin-top: 20px;\"><p style=\"font-family: sans-serif; \"><h1>Condizioni d'utilizzo dei cookies per questo sito</h1><p>Il sito internet "+ current_url +" fa uso di una tecnologia chiamata <i>cookie</i>, cio&egrave; piccoli file di testo che memorizzano sul browser corrente le preferenze dell'utente che visita il sito. I cookies sono utilizzati dalla maggior parte dei siti internet e sono generalmente una tecnologia sicura e stabile.<br><br>Questi cookies vengono da noi utilizzati al fine di:<ul><li>Personalizzare e migliorare l'esperienza complessiva dell'utente</li><li>Raccogliere dati statistici in forma anonima circa l'utilizzo del sito</li><li>Permettere l'accesso alle aree riservate del sito</li></ul>"+cookuse+"<br>Puoi disattivare l'utilizzo dei cookies in qualunque momento tramite le impostazioni del tuo browser</p></div><a href\"javascript:void();\" id=\"simpleCookieOkButton\" onclick=\"closeCookie()\" style=\"width: 250px;\">Accetto.<br>Chiudi questo messaggio</a>";
    x.style.height = '100%';
    x.innerHTML = txt;
}
openCookie();
